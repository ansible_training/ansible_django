from django.shortcuts import render
from django.views import View
from django.contrib.auth.models import User


class Home(View):
    template_name = 'home.html'

    def get(self, request):
        users = User.objects.all()
        context = {"users": users}
        return render(request, template_name=self.template_name, context=context)
